//Make connection

var socket = io.connect('http://localhost:4000');

var message = document.querySelector('#message');
var handle = document.querySelector('#handle');
var btn = document.querySelector('#send');
var output = document.querySelector('#output_msg');
var feedback = document.querySelector('#feedback');

// emit event

btn.addEventListener('click', ()=>{
    if (handle.value !== '' || message.value !== '') {
        socket.emit('chat', {
            message: message.value,
            handle: handle.value
        });
    }
    message.value = '';
    
});

message.addEventListener('keypress',()=>{
    socket.emit('typing',handle.value);
})

socket.on('chat', (data)=>{
    feedback.innerHTML = '';
    output.innerHTML += '<p><strong>'+data.handle+': </strong>'+data.message+'</p>';
})

socket.on('typing', (data)=>{
    feedback.innerHTML = '<p><em>'+data+' is typing message ...<em></p>';
})