var express = require('express');
var socket = require('socket.io');

var app = express();
var server = app.listen(4000, () =>{ 
    console.log('listening to requests on port 4000');
});


// static files

app.use(express.static('public'));

// socket setup

var io = socket(server);

io.on('connection', function(socket){
    socket.on('chat',(data)=>{
        io.sockets.emit('chat', data)
    });
    
    socket.on('typing',(data)=>{
        socket.broadcast.emit('typing', data);
    })
});